import org.junit.Test;
import utils.Coder;

/**
 * Created by Alex on 16/6/19.
 */
public class Base64Test {

    @Test
    public void decryptBASE64Test() throws Exception {
        byte[] decrypt = Coder.decryptBASE64("aGVsbG8gV29ybGQ=");
        System.out.println(new String(decrypt));
        //hello World
    }


    @Test
    public void encryptBASE64Test() throws Exception {
        String encrypt = Coder.encryptBASE64("hello World".getBytes());
        System.out.println(encrypt);

        //aGVsbG8gV29ybGQ=
    }
}
